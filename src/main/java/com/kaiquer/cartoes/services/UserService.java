package com.kaiquer.cartoes.services;

import com.kaiquer.cartoes.models.User;
import com.kaiquer.cartoes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public User create (User user) {
        return userRepository.save(user);
    }

    public User findById(int userId){
        return userRepository.findById(userId);
    }
}
