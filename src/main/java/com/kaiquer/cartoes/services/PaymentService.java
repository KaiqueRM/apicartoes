package com.kaiquer.cartoes.services;

import com.kaiquer.cartoes.models.Card;
import com.kaiquer.cartoes.models.Payment;
import com.kaiquer.cartoes.repository.CardRepository;
import com.kaiquer.cartoes.repository.PaymentRepository;
import com.kaiquer.cartoes.request.PaymentRequest;
import com.kaiquer.cartoes.response.PaymentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    CardRepository cardRepository;

    public PaymentResponse create(PaymentRequest paymentRequest) {
        Payment payment = paymentRequestToPayment(paymentRequest);

        if(!payment.getCard().isActive()) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Não foi possível realizar o pagamento, o cartão utilizado está inativo");
        }

        return paymentToPaymentResponse(paymentRepository.save(payment));
    }

    public List<PaymentResponse> listPaymentsByCardId(int idCard) {
        List<PaymentResponse> payments = new ArrayList<>();
        paymentRepository.findByCardId(idCard).forEach( payment -> {
            payments.add(paymentToPaymentResponse(payment));
        });

        return payments;
    }

    private PaymentResponse paymentToPaymentResponse(Payment payment) {
        if(payment == null) return null;

        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setId(payment.getId());
        paymentResponse.setCardId(payment.getCard().getId());
        paymentResponse.setDescription(payment.getDescription());
        paymentResponse.setPrice(payment.getPrice());

        return paymentResponse;
    }

    private Payment paymentRequestToPayment(PaymentRequest paymentRequest) {

        Card card = cardRepository.findById(paymentRequest.getCardId());
        Payment payment = new Payment();
        payment.setCard(card);
        payment.setDescription(paymentRequest.getDescription());
        payment.setPrice(paymentRequest.getPrice());

        return payment;
    }



}
