package com.kaiquer.cartoes.services;

import com.kaiquer.cartoes.models.Card;
import com.kaiquer.cartoes.models.User;
import com.kaiquer.cartoes.repository.CardRepository;
import com.kaiquer.cartoes.repository.UserRepository;
import com.kaiquer.cartoes.request.CardRequest;
import com.kaiquer.cartoes.response.CardResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class CardService {
    @Autowired
    CardRepository cardRepository;

    @Autowired
    UserRepository userRepository;

    public CardResponse create(CardRequest cardRequest) {
        User user = userRepository.findById(cardRequest.getUserId());

        if(user == null) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Usuário não encontrado para o ID "+user.getId());
        }

        Card card = cardRepository.findByNumber(cardRequest.getNumber());

        if(card != null) {
            throw new ResponseStatusException (HttpStatus.CONFLICT, "Já existe um cartão com o número " + card.getNumber());
        }

        card = new Card();
        card.setUser(user);
        card.setNumber(cardRequest.getNumber());
        card.setActive(false);

        card = cardRepository.save(card);

        return cardToCardResponse(card);
    }

    public CardResponse updateActive(String cardNumber, boolean active) {

        Card card = cardRepository.findByNumber(cardNumber);
        if(card == null) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Cartão não encontrado com o número " + cardNumber);
        }

        card.setActive(active);
        card = cardRepository.save(card);

        return cardToCardResponse(card);
    }

    public CardResponse findByNumber(String cardNumber) {
        CardResponse cardResponse = cardToCardResponse(cardRepository.findByNumber(cardNumber));
        if (cardResponse == null) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Cartão não encontrado com o número " + cardNumber);
        }
        return cardResponse;
    }

    private CardResponse cardToCardResponse(Card card) {

        if(card == null) return null;
        CardResponse cardResponse = new CardResponse();
        cardResponse.setId(card.getId());
        cardResponse.setActive(card.isActive());
        cardResponse.setNumber(card.getNumber());
        cardResponse.setUserId(card.getUser().getId());

        return cardResponse;
    }

}
