package com.kaiquer.cartoes.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardResponse {

    @JsonProperty(value = "id")
    private int id;

    @JsonProperty(value = "numero")
    private String number;

    @JsonProperty(value = "clienteId")
    private int userId;

    @JsonProperty(value = "ativo")
    private boolean active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
