package com.kaiquer.cartoes.repository;

import com.kaiquer.cartoes.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findById(int id);

}
