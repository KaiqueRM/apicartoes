package com.kaiquer.cartoes.repository;

import com.kaiquer.cartoes.models.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
    Payment findById(int id);

    @Query("SELECT p FROM Payment p JOIN p.card c WHERE c.id = ?1")
    List<Payment> findByCardId(int cardId);
}
