package com.kaiquer.cartoes.repository;

import com.kaiquer.cartoes.models.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CardRepository extends JpaRepository<Card, Long> {
    @Query("SELECT c FROM Card c WHERE c.number = ?1")
    Card findByNumber(String number);

    Card findById(int id);
}
