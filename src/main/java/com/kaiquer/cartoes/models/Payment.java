package com.kaiquer.cartoes.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="PAGAMENTO")
public class Payment implements Serializable {

    private static final long serialVersionUID = 3L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "id_cartao")
    private Card card;

    @Column(name = "descricao")
    private String description;

    @Column(name = "valor")
    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
