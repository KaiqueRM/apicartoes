package com.kaiquer.cartoes.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="CARTAO")
public class Card {

    private static final long serialVersionUID = 2L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    @Column(name="numero")
    private String number;

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private User user;

    @Column(name="ativo")
    private boolean isActive;

    @OneToMany(mappedBy = "card", cascade=CascadeType.ALL)
    Set<Payment> paymentHitSet;

    public int getId() { return id; }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
