package com.kaiquer.cartoes.resources;

import com.kaiquer.cartoes.models.User;
import com.kaiquer.cartoes.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="api")
public class UserResource {

    @Autowired
    UserService userService;

    @PostMapping("/user")
    public ResponseEntity newUser(@RequestBody User user) {
        return new ResponseEntity(userService.create(user), HttpStatus.CREATED);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity getUser(@PathVariable(value = "id") int id) {
        return new ResponseEntity(userService.findById(id), HttpStatus.OK);
    }
}
