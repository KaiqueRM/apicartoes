package com.kaiquer.cartoes.resources;

import com.kaiquer.cartoes.request.CardRequest;
import com.kaiquer.cartoes.services.CardService;
import org.omg.CORBA.RepositoryIdHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="api")
public class CardResource {

    @Autowired
    CardService cardService;

    @PostMapping("/cartao")
    public ResponseEntity newCard(@RequestBody CardRequest cardRequest) {
        return new ResponseEntity(cardService.create(cardRequest), HttpStatus.CREATED);
    }

    @PatchMapping("/cartao/{numero}")
    public ResponseEntity setActivate(@PathVariable(value = "numero") String cardNumber, @RequestBody CardRequest cardRequest) {
        return new ResponseEntity(cardService.updateActive(cardNumber, cardRequest.isActive()), HttpStatus.OK);
    }

    @GetMapping("/cartao/{numero}")
    public ResponseEntity getCardByNumber(@PathVariable(value = "numero") String cardNumber) {
        return new ResponseEntity(cardService.findByNumber(cardNumber), HttpStatus.OK);
    }
}
