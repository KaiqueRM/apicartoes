package com.kaiquer.cartoes.resources;

import com.kaiquer.cartoes.request.PaymentRequest;
import com.kaiquer.cartoes.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="api")
public class PaymentResource {

    @Autowired
    PaymentService paymentService;

    @PostMapping("/pagamento")
    public ResponseEntity newPayment(@RequestBody PaymentRequest paymentRequest) {
        return new ResponseEntity(paymentService.create(paymentRequest), HttpStatus.CREATED);
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public ResponseEntity getPaymentsByCard(@PathVariable(value = "id_cartao") int cardId) {
        return new ResponseEntity(paymentService.listPaymentsByCardId(cardId), HttpStatus.OK);
    }
}
