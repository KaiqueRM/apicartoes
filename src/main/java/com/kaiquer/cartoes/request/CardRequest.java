package com.kaiquer.cartoes.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardRequest {

    @JsonProperty(value = "numero")
    private String number;

    @JsonProperty(value = "clienteId")
    private int userId;

    @JsonProperty(value = "ativo")
    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
